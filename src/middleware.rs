use crate::to_login;
use tide::{Middleware, Next, Request};

pub(crate) struct AuthMiddleware;

#[derive(serde::Deserialize, serde::Serialize)]
pub struct UserInfo {
    pub(crate) username: String,
}

#[async_trait::async_trait]
impl<State> Middleware<State> for AuthMiddleware
where
    State: Clone + Send + Sync + 'static,
{
    async fn handle(&self, mut request: Request<State>, next: Next<'_, State>) -> tide::Result {
        let path = request.url().path().to_string();
        let session = request.session_mut();

        if session.get::<UserInfo>("user").is_some() {
            return Ok(next.run(request).await);
        }

        to_login(session, &path)
    }
}
